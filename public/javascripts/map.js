var map = L.map('main_map').setView([4.567676, -74.097771], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="htpps://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

//L.marker([4.564688, -74.087236]).addTo(map);
//L.marker([4.634168, -74.084669]).addTo(map);
 

$.ajax({
    dataType: "json",
    url="api/bicicletas",
    succes: function(result){
        console.log(result);
        result.bici.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map)
        });
    }
})