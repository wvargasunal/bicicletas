var express = require('express');
var router = express.Router();
var bicicletaController = require('../../controlers/api/bicicletaControllerApi');

router.get('/', bicicletaController.bicicleta_list);
router.post('/create', bicicletaController.Bicicleta_create);
module.exports = router;
