var Bicicleta = require('../../../modelo/bicicleta');
var request = require('request');
var server = require('../../../bin/www');


describe('Bicicleta Api', () => {
    describe('Get Bicic', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(2);

            request.get('http://http://localhost:4000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('Post bicicletas / create', () => {
        it('status 200', (done) => {
            var headers = {'content-type': 'application/json'};
            var aBici = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": 54}';
            request.post({
                headers: headers, 
                url: 'https://localhost:4000/api/bicicletas/create',
                body: aBici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.finById(10).color).toBe("rojo");
                done();
            });
        });
    });
        // describe("DELETE BICICLETAS /delete", () => {
    //     it("Status 204", (done) => {
    //         var a = Bicicleta.createInstance(1, "negro", "urbana", [-34.6012424, -58.3861]);
    //         Bicicleta.add(a, function (err, newBici) {
    //             var headers = {'content-type' : 'application/json'};
    //         });
    //     });
    // });
});