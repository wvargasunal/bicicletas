var mongoose = require('mongoose');
var Bicicleta = require('../../../modelo/bicicleta');
const bicicleta = require('../../../modelo/bicicleta');

describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = 'MONGODB://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are cnnected to test database !');
            done();
        });
    });
    afterEach(function(done) {
        Bicicleta.deleteMany({}, function( err, success){
            if(err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, 'verde', 'urbana', [-34.4, -54.8]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEquals(-34.6);
            expect(bici.ubicacion[1]).toEquals(-54.8);
        })
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add' , () => {
        it('agregas solo una bici', (done) => {
            var aBici = new Bicicleta({code:1, color: "verde", modelo: "urbana"});
            bicicleta.add(aBici, function(err, newBici) {
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){   
                expect(bicis.length).toEqual(1);
                expect(bicis[0].code).toEqual(aBici.code);

                done();
            });
          });
        });
    });
});
/*
beforeEach(() => { Bicicleta.allBicis = [];});
describe('Bicicleta.allBicis', () => {
  it('comienza vacia', () => {
      expect(Bicicleta.allBicis.length).toBe(0);
  })
});

describe('Bicicleta.add', () => {
    it('la cosa comienza en cero', () => {
    expect(Bicicleta.allBicis.length).toBe(0);


    expect(Bicicleta.allBicis.length).toBe(1);
    expect(Bicicleta.allBicis[0]).toBe(a);
    })
}); 


describe('Bicicleta.findById', () => {
    it('Debe devolver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, "verde", "urbana")
        Bicicleta.add(aBici);

        var targetBici = Bicicleta.finById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);
    })
})
*/