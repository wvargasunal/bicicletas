var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String, 
    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true}
}
})

bicicletaSchema.static.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletaSchema.methods.toString = function(){
    return 'code: ' + this.code + ' color: ' + this.color;
};

bicicletaSchema.methods.allBicis = function(cd){
    return this.find({}, cd);
};

bicicletaSchema.static.add = function(aBici, cb){
    this.create(aBici, cb);
};
module.exports = mongoose.model('Bicicleta', bicicletaSchema);

//var Bicicleta = function (id, color, modelo, ubicacion){
//    this.id = id;
//    this.color = color;
//    this.modelo = modelo;
//    this.ubicacion = ubicacion;
//}

//Bicicleta.allBicis = [];
//Bicicleta.add = function(aBici){
//    Bicicleta.allBicis.push(aBici);
//}
// Se crean registros por defecto para ejemplificar
//var a = new Bicicleta(1,'rojo','urbana',[4.634456, -74.083897]);
//var b = new Bicicleta(2,'negra','urbana',[4.623819, -74.065519]);
// Se agregar al array Bicicleta.allBicis mediante el metodo push
//Bicicleta.add(a);
//Bicicleta.add(b);

//Bicicleta.prototype.toString = function(){
 //   return 'id: ' + this.id + "color: "+ this.color; 
//}

//Bicicleta.finById = function(aBiciId){
//    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
//    if (aBici)
//       return aBici;
//    else
//    throw new Error(`No existe una bicicleta con el id ${aBiciId}`);   
//}

//Bicicleta.removeById = function (aBiciId){
//    for(var i = 0; i < Bicicleta.allBicis.length; i++){
//        if(Bicicleta.allBicis[i].id == aBiciId){
//            Bicicleta.allBicis.splice(i, 1);
//            break;
//        }
//    }
//}
// siempre se tiene que exportar el modelo para poder 
//utilizarlo desde otras carpetas
//module.exports = Bicicleta;