var Bicicleta = require('../modelo/bicicleta');

// Se utiliza el metodo esports para que la funcion
// pueda ser utlizada por otros archivos
exports.bicicleta_list = function(req, res) {
// Se renderea la vista index en la carpeta views -> bicicleta ->index.pug    
 res.render('bicicletas/index', {bicis: Bicicleta.allBicis});
}

// Funcion que carga la vista de creacion
exports.bicicleta_create_get = function(req, res){
     res.render('bicicletas/create');
}

// Funcion que crea la nueva bici
exports.bicicleta_create_post = function(req, res){
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo, req.body); 
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici)
    res.redirect('/bicicletas');
}

// Funcion que carga la vista de actualizacion
exports.bicicleta_update_get = function(req, res){
    var bici = Bicicleta.finById(req.params.id);
    res.render('bicicletas/update', {bici});
}

// Funcion que crea la nueva bici
exports.bicicleta_update_post = function(req, res){
   var bici = Bicicleta.finById(req.params.id);
   bici.id = req.body.id;
   bici.color = req.body.color;
   bici.modelo = req.body.modelo;
   bici.ubicacion = [req.body.lat, req.body.lng];
   
   res.redirect('/bicicletas');
}
exports.bicicleta_delete_post = function(req, res){
    Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');
}